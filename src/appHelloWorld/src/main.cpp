// SPDX-License-Identifier: MIT

/**
 * @file main.cpp
 * @author Laurent Hoeltgen <contact@laurenthoeltgen.name>
 * @date 2021
 * @copyright Copyright (c) 2021 Laurent Hoeltgen <contact@laurenthoeltgen.name>
 */

#include "main.hpp"

#include <fmt/core.h>

int
main()
{
        fmt::print("Hello, world!\n");
        print_hello_world();
        return 0;
}
