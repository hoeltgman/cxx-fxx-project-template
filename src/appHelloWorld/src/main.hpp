// SPDX-License-Identifier: MIT

/**
 * @file main.hpp
 * @author Laurent Hoeltgen <contact@laurenthoeltgen.name>
 * @date 2021
 * @copyright Copyright (c) 2021 Laurent Hoeltgen <contact@laurenthoeltgen.name>
 */

#ifndef INCLUDE_MAIN_HPP
#define INCLUDE_MAIN_HPP

extern "C"
{
        /**
         * @brief Print Hello World
         *
         */
        void
        print_hello_world();
}

#endif // #define INCLUDE_MAIN_HPP
