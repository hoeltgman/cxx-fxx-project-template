!! SPDX-License-Identifier: MIT

!> @file mod_hello_world_printer.f90
!! @author Laurent Hoeltgen <contact@laurenthoeltgen.name>
!! @date 2021
!! @copyright Copyright (c) 2021 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!!

!> @brief Module to collect functionality to print "Hello World!"
!! @details [Design](md_docs_design.html#111-interfacing)
!!
module hello_world_printer
    use iso_c_binding
    use iso_fortran_env, only: output_unit
    implicit none

contains

    !> @brief Print "Hello World!" to standard output.
    !! @details [Design](md_docs_design.html#121-output-target)
    !!
    subroutine print_hello_world() bind(C, name="print_hello_world")
        implicit none
        write (output_unit, *) "Hello World!"
    end subroutine

end module hello_world_printer
