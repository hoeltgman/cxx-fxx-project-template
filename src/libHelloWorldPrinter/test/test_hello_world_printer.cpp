// SPDX-License-Identifier: MIT

/**
 * @file test_hello_world_printer.cpp
 * @author Laurent Hoeltgen <contact@laurenthoeltgen.name>
 * @date 2021
 * @copyright Copyright (c) 2021 Laurent Hoeltgen <contact@laurenthoeltgen.name>
 * @details [Design](md_docs_design.html#111-interfacing)
 */

#include <gtest/gtest.h>

extern "C"
{
        void
        print_hello_world();
}

/**
 * @brief print "Hello World!"
 * @details [Design](md_docs_design.html#121-output-target)
 * @test Test that Hello World is printed.
 */
TEST(TestHelloWorldPrinter, PrintHelloWorld)
{
        print_hello_world();
        EXPECT_EQ(1, 1);
}
