add_compile_options (
    # Enable code coverage for gcc
    "$<$<AND:$<CONFIG:Debug>,$<BOOL:${ENABLE_COVERAGE}>,$<CXX_COMPILER_ID:GNU>>:--coverage>"
)

add_subdirectory ( libHelloWorldPrinter )

add_subdirectory ( appHelloWorld )
