# System Requirements Specification

- [System Requirements Specification](#system-requirements-specification)
  - [Definitions](#definitions)
    - [Definition 1](#definition-1)
  - [Requirements](#requirements)
    - [Requirement 1](#requirement-1)
    - [Requirement 2](#requirement-2)
    - [Requirement 3](#requirement-3)
    - [Requirement 4](#requirement-4)
    - [Requirement 5](#requirement-5)
    - [Requirement 6](#requirement-6)
  - [Design](#design)
    - [Use Cases](#use-cases)
    - [Components](#components)
    - [Activities](#activities)
    - [Classes and Modules](#classes-and-modules)

<a id="definitions"></a>
## Definitions

<a id="definition-1"></a>
### Definition 1

_Standard output_ is a stream to which a program writes its output data. Unless redirected, standard output is inherited from the parent process. In the case of an interactive shell, that is usually the text terminal which initiated the program.

<a id="requirements"></a>
## Requirements

<a id="requirement-1"></a>
### Requirement 1
`product requirement` | `system requirement` | `functional requirement`

The system shall output the text _Hello World!_ on standard output in a command line window.

Refs: [Definition 1](#definition-1)
Design: [Use Case](#use-cases)
Testing: [Specification](test.html#_test000001) [Evaluation](Unit.html)

<a id="requirement-2"></a>
### Requirement 2
`product requirement` | `system requirement` | `non-functional requirement`

The system shall consist of the following components:
- a static library
- a command line application

Design: [Components](#components)

<a id="requirement-3"></a>
### Requirement 3
`product requirement` | `software requirement` | `functional requirement`

The static library shall contain the functionality to output the text _Hello World!_ on standard output.

Requirements: [Requirement 2](#requirement-2)
Design: [Components](#components) | [Activities](#activities)

<a id="requirement-4"></a>
### Requirement 4
`product requirement` | `software requirement` | `non-functional requirement`

The static library shall have a C compatible interface.

<a id="requirement-5"></a>
### Requirement 5
`process requirement`

The static library shall be written in Fortran 2008.

<a id="requirement-6"></a>
### Requirement 6
`process requirement`

The command line application shall be written in C++17.

<a id="design"></a>
## Design

<a id="use-cases"></a>
### Use Cases

\startuml
actor User << Human >> as user
usecase "Receive\n"Hello World!"\nmessage." as case
user - case
url of case is [[md_docs_design.html#requirement-1{Requirement 1}]]
\enduml

Requirements: [Requirement 1](#requirement-1)

### Components

\startuml
() STDOUT
package "System" {
component HelloWorldPrinter
component HelloWorld
}

HelloWorld - HelloWorldPrinter
HelloWorldPrinter ..> STDOUT : use
url of HelloWorldPrinter is [[md_docs_design.html#requirement-2{Requirement 2}]]
\enduml

Requirements: [Requirement 2](#requirement-2) | [Requirement 3](#requirement-3)

### Activities

\startuml
HelloWorld -> HelloWorldPrinter : print_hello_world
url of HelloWorldPrinter is [[md_docs_design.html#requirement-1{Requirement 1}]]
\enduml

\startuml
start
[[namespacehello__world__printer.html#requirement-2{Requirement 2}]]:Print "Hello World" to STDOUT;
end
\enduml

Design: [Use Cases](#use-cases)

### Classes and Modules

\startuml
class hello_world_printer [[namespacehello__world__printer.html#requirement-2{Requirement 2}]]

class hello_world_printer {
    +print_hello_world()
}
\enduml

Design: [Components](#components)
