# cxx-fxx-project-template

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![license](https://img.shields.io/badge/license-MIT-green)](https://opensource.org/licenses/MIT)

A project scaffold for new c++ and/or fortran projects.

## Features

- .devcontainer setup which proivdes c/c++ compilers as well as fortran compiler and related tools
- vcpkg support: add your dependencies to the vcpkg.json
- cmake support: add your configurations to the CMakePresets.json or CMakeUserPresets.json
- git lint support to enforce conventional commits
- pre-commit hooks to enforce proper code via linters
- some dummy c++ and fortran code to create a hello world app, a corresponding library and unit tests
- clang-format and fprettify support to get consistent code formatting
- Sample markdown/plantuml files on how a docs-as-code approach for requirements could look like. The final documentation can be generated with Doxygen.
